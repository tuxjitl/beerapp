create table if not exists Brewers
(
    Id integer auto_increment
        primary key,
    Name varchar(50) null,
    Address varchar(50) null,
    ZipCode varchar(6) null,
    City varchar(50) null,
    Turnover int default 0 null
);


create table if not exists Categories
(
    Id integer auto_increment
        primary key,
    CategoryName varchar(50) null
);

create table if not exists Beers
(
    Id integer auto_increment
        primary key,
    Name varchar(100) null,
    BrewerId integer null,
    CategoryId integer null,
    Price float default 0 null,
    Stock int default 0 null,
    Alcohol float default 0 null,
    Version int default 0 null,
    constraint Beers_Brewer
        foreign key (BrewerId) references Brewers (Id),
    constraint Beers_Category
        foreign key (CategoryId) references Categories (Id)
);

create table if not exists BeerOrders
(
    Id   integer auto_increment
        primary key,
    Name varchar(100) null
);

create table if not exists BeerOrderItems
(
    Id integer auto_increment
        primary key,
    BeerOrderId integer null,
    BeerId integer null,
    Number int default 0 null,
    constraint BeerOrderItems_BeerOrders
        foreign key (BeerOrderId) references BeerOrders (Id),
    constraint BeerOrderItems_Beers
        foreign key (BeerId) references Beers (Id)
);




