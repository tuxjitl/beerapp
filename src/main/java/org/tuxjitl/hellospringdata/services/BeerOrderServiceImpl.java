package org.tuxjitl.hellospringdata.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tuxjitl.hellospringdata.model.BeerOrder;
import org.tuxjitl.hellospringdata.repositories.BeerOrderRepository;

import java.util.Optional;

@Service
public class BeerOrderServiceImpl implements BeerOrderService {
    BeerOrderRepository beerOrderRepo;

    @Autowired
    public BeerOrderServiceImpl(BeerOrderRepository beerOrderRepo) {
        this.beerOrderRepo = beerOrderRepo;
    }

    @Override
    public void placeOrder(BeerOrder beerOrder) {
        beerOrderRepo.save(beerOrder);
    }

    @Override
    public Optional<BeerOrder> retrieveOrderById(Long id){
        return beerOrderRepo.findById(id);
    }
}
