package org.tuxjitl.hellospringdata.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.tuxjitl.hellospringdata.model.Category;
import org.tuxjitl.hellospringdata.repositories.CategoryRepository;

import java.util.List;

public class CategoryServiceImpl implements CategoryService{

    CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository) {

        this.categoryRepository = categoryRepository;
    }

    @Override
    public Category retrieveCategoryById(Long id) {

        Category catFound = categoryRepository.findById(id).orElseThrow(RuntimeException::new);
        return catFound;
    }

    @Override
    public List<Category> retrieveAll() {

        return categoryRepository.findAll();
    }

    @Override
    public void register(Category category) {
        categoryRepository.save(category);
    }
}
