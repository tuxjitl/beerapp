package org.tuxjitl.hellospringdata.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.tuxjitl.hellospringdata.model.Brewer;
import org.tuxjitl.hellospringdata.repositories.BrewerRepository;

import java.util.List;

public class BrewerServiceImpl implements BrewerService{

    BrewerRepository brewerRepository;

    @Autowired
    public BrewerServiceImpl(BrewerRepository brewerRepository) {

        this.brewerRepository = brewerRepository;
    }

    @Override
    public Brewer retrieveBrewerById(Long id) {
        Brewer foundBrewer = brewerRepository.findById(id).orElseThrow(RuntimeException::new);
        return foundBrewer;
    }

    @Override
    public List<Brewer> retrieveBrewers() {

        return brewerRepository.findAll();
    }

    @Override
    public void register(Brewer brewer) {
        brewerRepository.save(brewer);
    }
}
