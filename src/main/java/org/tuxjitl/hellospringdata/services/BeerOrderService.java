package org.tuxjitl.hellospringdata.services;

import org.tuxjitl.hellospringdata.model.BeerOrder;

import java.util.Optional;

public interface BeerOrderService {
    void placeOrder(BeerOrder beerOrder);
    Optional<BeerOrder> retrieveOrderById(Long id);
}

