package org.tuxjitl.hellospringdata.services;

import org.tuxjitl.hellospringdata.model.Beer;

import java.util.List;

public interface BeerService {
    Beer retrieveBeerById(Long id);
    List<Beer> retrieveBeers();
    void register(Beer beer);
}
