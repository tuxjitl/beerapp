package org.tuxjitl.hellospringdata.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.tuxjitl.hellospringdata.model.Beer;
import org.tuxjitl.hellospringdata.repositories.BeerRepository;

import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {
    BeerRepository beerRepo;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepo) {
        this.beerRepo = beerRepo;
    }

    @Override
    public Beer retrieveBeerById(Long id) {
        Beer foundBeer = beerRepo.findById(id).orElseThrow(RuntimeException::new);
        return foundBeer;
    }

    @Override
    public List<Beer> retrieveBeers() {

        return beerRepo.findAll();
    }

    @Override
    public void register(Beer beer) {
        beerRepo.save(beer);
    }
}
