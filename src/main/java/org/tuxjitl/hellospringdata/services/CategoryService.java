package org.tuxjitl.hellospringdata.services;

import org.tuxjitl.hellospringdata.model.Category;

import java.util.List;

public interface CategoryService {

    Category retrieveCategoryById(Long id);
    List<Category> retrieveAll();
    void register(Category category);

}
