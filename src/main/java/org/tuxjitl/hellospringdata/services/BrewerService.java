package org.tuxjitl.hellospringdata.services;

import org.tuxjitl.hellospringdata.model.Brewer;

import java.util.List;

public interface BrewerService {

    Brewer retrieveBrewerById(Long id);
    List<Brewer> retrieveBrewers();
    void register(Brewer brewer);
}
