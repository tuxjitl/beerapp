package org.tuxjitl.hellospringdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.tuxjitl.hellospringdata.model.Beer;
import org.tuxjitl.hellospringdata.model.BeerOrder;
import org.tuxjitl.hellospringdata.model.BeerOrderItem;
import org.tuxjitl.hellospringdata.services.BeerOrderService;
import org.tuxjitl.hellospringdata.services.BeerOrderServiceImpl;
import org.tuxjitl.hellospringdata.services.BeerService;
import org.tuxjitl.hellospringdata.services.BeerServiceImpl;

@SpringBootApplication
public class HelloSpringDataApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(HelloSpringDataApplication.class, args);



        BeerService beerService = ctx.getBean("beerServiceImpl", BeerServiceImpl.class);


//        beerService.retrieveBeers().forEach(System.out::println);


        Beer beer = (Beer) beerService.retrieveBeerById(60L);
        System.out.println(beer);


        // Placing order...
        BeerOrderService beerOrderService = ctx.getBean("beerOrderServiceImpl", BeerOrderServiceImpl.class);
        BeerOrder myOrder = new BeerOrder();
        myOrder.setName("JC laChouf 2");

        BeerOrderItem myOrderItem = new BeerOrderItem();
        myOrderItem.setBeer(beer);
        myOrderItem.setNumber(57);
        myOrder.addOrderItem(myOrderItem);
        beerOrderService.placeOrder(myOrder);

        Long beerOrderId = 37L;
        System.out.printf("Beer order id: %s%n" +
                "%s%n", beerOrderId, beerOrderService.retrieveOrderById(beerOrderId));


    }
}
