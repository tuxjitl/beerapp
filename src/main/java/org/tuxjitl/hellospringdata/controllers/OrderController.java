package org.tuxjitl.hellospringdata.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tuxjitl.hellospringdata.repositories.BeerOrderRepository;

@Controller
@RequestMapping({"/orders"})
public class OrderController {
    private BeerOrderRepository beerOrderRepository;


    public OrderController(BeerOrderRepository beerOrderRepository) {

        this.beerOrderRepository = beerOrderRepository;
    }

    @GetMapping()
    public String getOrders(Model model){
        model.addAttribute("orders", beerOrderRepository.findAll());

        return "orders";
    }
}
