package org.tuxjitl.hellospringdata.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tuxjitl.hellospringdata.repositories.CategoryRepository;

@Controller
@RequestMapping({"/categories"})
public class CategoryController {

    private CategoryRepository categoryRepository;

    public CategoryController(CategoryRepository categoryRepository) {

        this.categoryRepository = categoryRepository;
    }

    @GetMapping()
    public String getCategories(Model model){
        model.addAttribute("categories",categoryRepository.findAll());

        return "categories";
    }


}
