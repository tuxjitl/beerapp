package org.tuxjitl.hellospringdata.controllers;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Component
@RequestMapping({"","/","/index","/index.html"})
public class IndexController {

    @GetMapping()
    public String indexPage(){
        return "index";
    }

}
