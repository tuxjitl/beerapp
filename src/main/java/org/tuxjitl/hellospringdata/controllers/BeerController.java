package org.tuxjitl.hellospringdata.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.tuxjitl.hellospringdata.repositories.BeerRepository;

@Controller
@RequestMapping({"/beers"})
public class BeerController {

    private BeerRepository beerRepository;

    public BeerController(BeerRepository beerRepository) {

        this.beerRepository = beerRepository;
    }

    @GetMapping()
    public String getBeers(Model model){

        //add a list of beers out of the repository to the model
        model.addAttribute("beers",beerRepository.findAll());

        //for thymeleaf use: associate this string with the thymeleaf view called books
        return "beers";

    }

}
