package org.tuxjitl.hellospringdata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Beers")
@Getter
@Setter
public class Beer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Price")
    private double price;
    @Column(name = "Stock")
    private int stock;
    @Column(name = "Alcohol")
    private float alcohol;
    @Column(name = "Version")
    private int version;
    @ManyToOne
    @JoinColumn(name = "CategoryId")
    private Category category;
    @ManyToOne
    @JoinColumn(name = "BrewerId")
    private Brewer brewer;


    public Beer(Long id, String name, double price, int stock, float alcohol, int version, Category category, Brewer brewer) {

        this.id = id;
        this.name = name;
        this.price = price;
        this.stock = stock;
        this.alcohol = alcohol;
        this.version = version;
        this.category = category;
        this.brewer = brewer;
    }

    public Beer() {


    }

    public Beer(String name){
        this.name=name;
    }

    @Override
    public String toString() {
        return "Beer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", alcohol=" + alcohol +
                ", version=" + version +
                ", category=" + category.getName() +
                ", brewer=" + brewer.getName() +
                '}';
    }
}
