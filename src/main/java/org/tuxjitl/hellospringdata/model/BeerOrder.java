package org.tuxjitl.hellospringdata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "BeerOrders")
@Getter
@Setter
public class BeerOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;
    @Column(name = "Name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JoinColumn(name = "BeerOrderId")
    private List<BeerOrderItem> items = new ArrayList<>();

    public void addOrderItem(BeerOrderItem orderItem){
        this.items.add(orderItem);
    }

    @Override
    public String toString() {
        StringBuilder stringAsSB = new StringBuilder("BeerOrder{" +
               "id=" + id +
                ", name='" + name + '\'');

        for (BeerOrderItem item : items) {
            stringAsSB.append("{").append(item.getNumber()).append("x").append(item.getBeer().getName()).append("}, ");
        }
        return stringAsSB.toString();

    }


}
