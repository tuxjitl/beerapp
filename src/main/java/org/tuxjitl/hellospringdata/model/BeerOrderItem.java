package org.tuxjitl.hellospringdata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "BeerOrderItems")
@Getter
@Setter
public class BeerOrderItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "BeerId")
    private Beer beer;
    @Column(name = "Number")
    private int number;
}
