package org.tuxjitl.hellospringdata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "Categories")
@Getter
@Setter

public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;
    @Column(name = "CategoryName")
    private String name;
    @OneToMany(mappedBy = "category")
    private Set<Beer> beers;

    public Category(Long id, String name) {

        this.id = id;
        this.name = name;
    }

    public Category(long id, String alcoholarm) {

    }

    public Category() {


    }
}
