package org.tuxjitl.hellospringdata.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Brewers")
@Getter
@Setter
public class Brewer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;
    @Column(name = "Name")
    private String name;
    @Column(name = "Address")
    private String address;
    @Column(name = "ZipCode")
    private String zipCode;
    @Column(name = "City")
    private String city;
    @Column(name = "Turnover")
    private int turnover;
    @OneToMany(mappedBy = "brewer")
    private Set<Beer> beers = new HashSet<>();

    public Brewer(Long id, String name, String address, String zipCode, String city, int turnover) {

        this.id = id;
        this.name = name;
        this.address = address;
        this.zipCode = zipCode;
        this.city = city;
        this.turnover = turnover;
    }

    public Brewer() {


    }
}
