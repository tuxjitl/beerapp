package org.tuxjitl.hellospringdata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.tuxjitl.hellospringdata.model.BeerOrder;

public interface BeerOrderRepository extends JpaRepository<BeerOrder,Long> {

}
