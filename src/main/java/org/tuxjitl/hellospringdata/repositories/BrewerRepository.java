package org.tuxjitl.hellospringdata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tuxjitl.hellospringdata.model.Brewer;

@Repository
public interface BrewerRepository extends JpaRepository<Brewer,Long> {
}
