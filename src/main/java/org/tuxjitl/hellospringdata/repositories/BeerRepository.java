package org.tuxjitl.hellospringdata.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.tuxjitl.hellospringdata.model.Beer;

@Repository
public interface BeerRepository extends JpaRepository<Beer,Long> {

}
